using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetector : MonoBehaviour
{
	[SerializeField] private PlayerJump playerJump;

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.transform.parent.CompareTag("WhitePlayer") || (collision.transform.parent.CompareTag("BlackPlayer")))
		{
			return;
		}

		playerJump.OnGround();
	}
	private void OnTriggerExit2D(Collider2D collision)
	{
		playerJump.NotOnGround();
	}

	private void OnCollisionExit2D(Collision2D collision)
	{

	}
}
