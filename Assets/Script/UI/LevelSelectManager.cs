using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour
{
    [SerializeField] private GameObject buttonSorter;
    [SerializeField] private SceneChange sceneLoader;
    [SerializeField] private List<Sprite> imageList = new List<Sprite>();
    [SerializeField] private Image previewImage;
    // Start is called before the first frame update
    void Start()
    {

        sceneLoader = GetComponent<SceneChange>();

        for(int i = 0; i <  buttonSorter.transform.childCount; i++)
		{
            var child = buttonSorter.transform.GetChild(i);
            int buttonNumber = i;
            child.GetChild(0).GetComponent<TextMeshProUGUI>().text = "LEVEL " + (i +1).ToString();
            child.GetComponent<Button>().onClick.AddListener(() => sceneLoader.ChangeScene("Level" + (buttonNumber +1).ToString())) ;
        }
    }

    public void changePreviewImage(int index)
	{
        previewImage.sprite = imageList[index];
	}



   
}
