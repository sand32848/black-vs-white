using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField] private GameObject lostUI;
    [SerializeField] private GameObject winUI;
    [SerializeField] private GameObject pauseUI;

	private void OnEnable()
	{
		Red.blackHit += callWinScreen;
		Red.whiteHit += callLoseScreen;
		GameManager.pause += callPause;


	}

	private void OnDisable()
	{
		Red.blackHit -= callWinScreen;
		Red.whiteHit -= callLoseScreen;
		GameManager.pause -= callPause;

	}
	private void Start()
	{
		winUI.SetActive(false);
		lostUI.SetActive(false);
	}
	public void callLoseScreen()
	{
		if (!winUI.activeSelf)
		{
			lostUI.SetActive(true);
		}
	}

	public void callWinScreen()
	{
		winUI.SetActive(true);
	}

	public void callPause()
	{
		if (!pauseUI.activeSelf)
		{
			pauseUI.SetActive(true);
		}
		else
		{
			pauseUI.SetActive(false);
		}
	
	}
}
