using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using DG.Tweening;

public class InputController : MonoBehaviour
{
    public static InputController _instance;
    public static InputController Instance => _instance;

    public InputMaster inputMaster;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        inputMaster = new InputMaster();
    }
    private void OnEnable()
    {
        inputMaster.Enable();
    }
    private void OnDisable()
    {
        inputMaster.Disable();
    }

    public float movement()
	{
        float _float = 0;
        if (inputMaster.PlayerAction.Movement.ReadValue<Vector2>().x > 0.0f)
        {
            _float = 1;
        }
        else if (inputMaster.PlayerAction.Movement.ReadValue<Vector2>().x < 0.0f)
	    {
            _float = -1;
        }
	    else
	    {
            _float = 0;
        }

        return _float;
    }
  
    public bool W => inputMaster.PlayerAction.Jump.triggered;

    public bool ESC => inputMaster.ESC.Pause.triggered;
    //public bool LeftHold => inputMaster.PlayerAction.LeftHold.ReadValue<float>() > 0.1f;
    //public Vector2 mousePos => inputMaster.PlayerAction.MousePos.ReadValue<Vector2>();
    //public bool R => inputMaster.PlayerAction.Restart.triggered;
    //public bool ESC => inputMaster.UI.Esc.triggered;
    //public bool LeftClick => inputMaster.PlayerAction.LeftClick.triggered;


    //public float RightHold => inputActions.PlayerControl.RightHold.ReadValue<float>();
    //public bool LeftClick => inputActions.PlayerControl.LeftClick.triggered;
    //public bool RightClick => inputActions.PlayerControl.RightClick.triggered;

    //public bool Spacebar => inputActions.PlayerControl.Spacebar.triggered;
    //public bool Console => inputActions.PlayerControl.Console.triggered;

    //public void EnableMouseLook() => inputActions.PlayerControl.MouseLook.Enable();

    //public void DisableMouseLook() => inputActions.PlayerControl.MouseLook.Disable();

    public void enableInput()
	{
        inputMaster.PlayerAction.Enable();
	}

    public void disableInput()
	{
        inputMaster.PlayerAction.Disable();
    }
}
