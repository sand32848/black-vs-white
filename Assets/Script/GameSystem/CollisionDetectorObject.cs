using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class CollisionDetectorObject : MonoBehaviour
{
	[SerializeField] private UnityEvent<GameObject> unityEvent;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		unityEvent.Invoke(collision.gameObject);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		unityEvent.Invoke(collision.transform.gameObject);
	}
}
