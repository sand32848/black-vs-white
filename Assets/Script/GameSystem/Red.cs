using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Red : MonoBehaviour
{
	public static Action whiteHit;
	public static Action blackHit;
	public static Action hit;
	// Start is called before the first frame update

	public void redHit(GameObject _object)
	{
		hit?.Invoke();

		StartCoroutine(delayCall(_object));

		if (_object.TryGetComponent(out PlayerDeath playerDeath))
		{
			playerDeath.Death();
		}
	}

	IEnumerator delayCall(GameObject d)
	{
		yield return new WaitForSeconds(1f);

		if (d.CompareTag("WhitePlayer"))
		{
			whiteHit?.Invoke();
		}
		else
		{
			blackHit?.Invoke();

		}

	
	}
}
