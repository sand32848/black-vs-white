using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class CollisionDetectorGeneric : MonoBehaviour
{
	[SerializeField] private UnityEvent unityEvent;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		unityEvent.Invoke();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		unityEvent.Invoke();
	}
}
