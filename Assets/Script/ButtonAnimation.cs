using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(EventTrigger))]
public class ButtonAnimation : MonoBehaviour
{
	[SerializeField] private float scale;
	[SerializeField] private float originalScale;
	[SerializeField] private float shrinkSize;
	EventTrigger eventTrigger;
	Button button;

	private void Start()
	{
		originalScale = transform.localScale.x;
		button = GetComponent<Button>();
		eventTrigger = GetComponent<EventTrigger>();

		EventTrigger.Entry pointerHover = new EventTrigger.Entry();
		pointerHover.eventID = EventTriggerType.PointerEnter;
		pointerHover.callback.AddListener((data) => { onPointerHoverDelegate((PointerEventData)data); });
		eventTrigger.triggers.Add(pointerHover);

		EventTrigger.Entry PointerExit = new EventTrigger.Entry();
		PointerExit.eventID = EventTriggerType.PointerExit;
		PointerExit.callback.AddListener((data) => { onPointerExitDelegate((PointerEventData)data); });
		eventTrigger.triggers.Add(PointerExit);

		EventTrigger.Entry pointerEnter = new EventTrigger.Entry();
		pointerEnter.eventID = EventTriggerType.PointerDown;
		pointerEnter.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
		eventTrigger.triggers.Add(pointerEnter);

		EventTrigger.Entry pointerClick = new EventTrigger.Entry();
		pointerClick.eventID = EventTriggerType.PointerClick;
		pointerClick.callback.AddListener((data) => { OnPointerClickDelegate((PointerEventData)data); });
		eventTrigger.triggers.Add(pointerClick);
	}
	public void ButtonHover()
	{
		transform.DOScale(scale, 0.3f).SetEase(Ease.OutBack).SetUpdate(true);
	}

	public void ButtonExit()
	{

		transform.DOScale(originalScale, 0.3f).SetUpdate(true);
	}

	public  void onPointerHoverDelegate(PointerEventData data)
	{
		if (button.interactable == false)
		{
			return;
		}
		ButtonHover();
	}

	public void onPointerExitDelegate(PointerEventData data)
	{
		if (button.interactable == false)
		{
			return;
		}
		ButtonExit();
	}

	public void OnPointerClickDelegate(PointerEventData data)
	{
		if (button.interactable == false)
		{
			return;
		}
		AudioManager.instance?.Play("ButtonClick");

	}

	public void OnPointerDownDelegate(PointerEventData data)
	{
		transform.DOScale(0.9f, 0.3f).SetUpdate(true);
	}
}
