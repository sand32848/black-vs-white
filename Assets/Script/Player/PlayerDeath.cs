using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
	[SerializeField] GameObject deathParticle;


   public void Death()
	{
		gameObject.SetActive(false);
		AudioManager.instance?.Play("Dead");

		if (deathParticle)
		{
			Instantiate(deathParticle, transform.position, Quaternion.identity);
		}
	}
}
