using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    [SerializeField] private float Speed;
    private Rigidbody2D rb;

	private void OnEnable()
	{
		InputController.Instance.enableInput();
		Red.hit += disableMovmenet;
	}

	private void OnDisable()
	{
		InputController.Instance.disableInput();
		Red.hit -=  disableMovmenet;
	}

	// Start is called before the first frame update
	void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

	private void FixedUpdate()
	{
        DoMovement(InputController.Instance.movement());
    }
	// Update is called once per frame

    private void DoMovement(float vector)
	{
        rb.velocity = new Vector2(vector * Speed, rb.velocity.y)  ;
	}

	private void disableMovmenet()
	{
		InputController.Instance.disableInput();
		rb.simulated = false;
	}
}
