using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float jumpForce;
    [SerializeField] private LayerMask layerMask;
    private bool isGround = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
    }

   private void Jump()
	{
		if (!isGround)
		{
            return;
		}

		if (InputController.Instance.W)
		{
            rb.velocity = new Vector2 (rb.velocity.x , jumpForce);
		}
	}

    public void OnGround()
	{
        isGround = true;
    }

    public void NotOnGround()
    {
        isGround = false;
    }
}
