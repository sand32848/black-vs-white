using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class cameraController : MonoBehaviour
{
	private void OnEnable()
	{
		Red.hit += shake;
	}

	private void OnDisable()
	{
		Red.hit -= shake;
	}
	// Start is called before the first frame update
	public void shake()
	{
		transform.DOShakePosition(1, 0.5f, 10, 90, false, true);
	}
}
